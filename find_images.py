#! /usr/bin/env python3

import tensorflow as tf
import numpy as np
from sklearn.neighbors import NearestNeighbors
import skimage.io
import skimage.transform
from matplotlib import pyplot as plt
from multiprocessing import Pool
import os

shape = (256, 256)
result_limit = 20
# dataset_dir = './test_cases/christmas_tree/all_images'
# output_dir = './test_cases/christmas_tree/outputs'
# input_dir = './test_cases/christmas_tree/images_to_find'

dataset_dir = './test_cases/tiger/zoo_images'
input_dir = './test_cases/tiger/tiger_query_image'
output_dir = './test_cases/tiger/outputs'


def read_and_resize_image(path):
    try:
        image = skimage.io.imread(path)
        return (image, skimage.transform.resize(image, shape))
    except Exception as e:
        print(path + ' bad image')
        return None

# Read images with common extensions from a directory


def read_imgs_dir(dirPath):
    paths = [os.path.join(dirPath, filename)
             for filename in os.listdir(dirPath)]
    pool = Pool()
    imgs = pool.map(read_and_resize_image, paths)
    pool.close()
    pool.join()
    res_img = []
    res_resized = []
    for (img, resized_img) in imgs:
        if resized_img is None:
            continue
        if resized_img.shape != shape + (3, ):
            continue
        res_img.append(img)
        res_resized.append(resized_img)
    return res_img, res_resized


def main():
    all_images, resized_all_images = read_imgs_dir(dataset_dir)
    images_to_find, resized_images_to_find = read_imgs_dir(input_dir)
    os.makedirs(output_dir, exist_ok=True)

    train_images = np.array(resized_all_images)
    test_images = np.array(resized_images_to_find)

    # model = tf.keras.applications.VGG19(
    #     weights='imagenet', include_top=False, input_shape=shape + (3, ))

    # model = tf.keras.applications.NASNetLarge(include_top=False,
    #                                           weights='imagenet', input_shape=shape + (3,))

    model = tf.keras.applications.ResNet152V2(
        include_top=False, weights='imagenet', input_shape=shape + (3, ))

    model.summary()

    model_output_shape = model.output.shape[1:]

    print(model_output_shape)

    print(train_images.shape)
    print(test_images.shape)

    # X_train = tf.keras.applications.vgg19.preprocess_input(train_images)
    # X_test = tf.keras.applications.vgg19.preprocess_input(test_images)

    # X_train = tf.keras.applications.nasnet.preprocess_input(train_images)
    # X_test = tf.keras.applications.nasnet.preprocess_input(test_images)

    X_train = tf.keras.applications.resnet_v2.preprocess_input(train_images)
    X_test = tf.keras.applications.resnet_v2.preprocess_input(test_images)

    E_train = model.predict(X_train)
    E_test = model.predict(X_test)
    print(E_train.shape)
    print(E_test.shape)

    E_train_flat = np.reshape(E_train, (-1, np.prod(model_output_shape)))
    E_test_flat = np.reshape(E_test, (-1, np.prod(model_output_shape)))

    print(E_train_flat.shape)
    print(E_test_flat.shape)

    knn = NearestNeighbors(n_neighbors=result_limit)
    knn.fit(E_train_flat)
    for (i, emb) in enumerate(E_test_flat):
        _, indices = knn.kneighbors([emb])
        print(indices)
        for indice in indices[0]:
            image = all_images[indice]
            plt.imsave(os.path.join(output_dir, f"{indice}.png"), image)


if __name__ == '__main__':
    main()
